<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//I dont see a need to create another controller just for this route. and definitely not using closure here haha
Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('landing');

Route::post('post', [\App\Http\Controllers\PostController::class, 'store'])->name('post.store')->middleware('auth');
Route::get('post/like/{post_id}', [\App\Http\Controllers\PostController::class, 'like'])->name('post.like')->middleware('auth');

Route::get('account-posts', [\App\Http\Controllers\PostController::class, 'index'])->name('post.account.posts')->middleware('auth');
Route::put('account-posts/{post}', [\App\Http\Controllers\PostController::class, 'update'])->name('post.account.posts.update')->middleware('auth');
Route::get('account-posts/delete/{post}', [\App\Http\Controllers\PostController::class, 'destroy'])->name('post.account.posts.destroy')->middleware('auth');

Route::post('comment/{post_id}', [\App\Http\Controllers\CommentController::class, 'store'])->name('comment.store')->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
