@extends('layouts.app')


@section('main-content')

    <div class="card-wrapper flex flex-col items-center justify-center w-full space-y-8 bg-gray-100">

        <div class="alerts w-1/2">
            @if(session()->has('posted'))
            <div class="success-alert text-lg w-full bg-green-500 text-white p-2 rounded-md shadow-md">{{session()->get('posted')}}</div>
            @endif

            @if(session()->has('warning'))
            <div class="warning-alert text-lg w-full bg-yellow-500 text-white p-2 rounded-md shadow-md">{{session()->get('warning')}}</div>
            @endif

            @if(session()->has('error'))
            <div class="danger-alert text-lg w-full bg-red-500 text-white p-2 rounded-md shadow-md">{{session()->get('error')}}</div>
            @endif

            @if ($errors->any())
                <div class="warning-alert text-lg w-full bg-yellow-500 text-white p-2 rounded-md shadow-md">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
        </div>

        <div class="post-card bg-gray-500 lg:w-1/2 w-full rounded-md shadow-md p-3 h-auto">

            <div class="main-body">

                <div class="input-box mt-4">

                    <div class="post-input p-2">

                        <form action="{{route('post.store')}}" method="POST" enctype="multipart/form-data">

                            @csrf

                            <textarea name="content" class="rounded-sm shadow-md w-full p-1 pl-2 bg-gray-100" placeholder="Write something amazing. You can either post a picture post or text post, or both..."></textarea>

                            <div class="image-selection">
                                <label for="image">
                                    <img src="{{asset('images/misc/image_selection.svg')}}" alt="" class="w-8 cursor-pointer">
                                </label>
                                <input type="file" name="image" id="image" class="hidden">
                            </div>

                            <div class="post mt-4">
                                <label for="submit">
                                    <img src="{{asset('images/misc/send.svg')}}" alt="" class="w-8 cursor-pointer">
                                </label>
                                <input type="submit" id="submit" value="Post" class="bg-blue-500 p-1 text-white rounded-md hidden">
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

        @foreach($posts as $post)
        <div class="single-post bg-gray-500 lg:w-1/2 w-full rounded-md shadow-md p-3 h-auto" id="post_{{$post->id}}">

            <div class="header">
                <div class="user-holder flex space-x-3 items-center">
                    <img src="{{asset('images/misc/user.svg')}}" alt="" class="w-12">
                    <div class="w-full">
                        <p class="user-name font-bold text-xl text-white">{{$post->user->name}}</p>
                        <p class="posted-date text-xs text-white">{{$post->created_at->toFormattedDateString()}}</p>
                    </div>
                </div>
            </div>

            <div class="content mt-2">

                <img src="{{asset('images/uploads/').'/'.$post->image}}" alt="" class="mb-2 rounded-md">

                <span class="text-white">{{$post->content}}</span>

            </div>

            <div class="footer mt-3">

                <div class="action-buttons flex justify-end">
                    <div class="footer-item flex items-center justify-center space-x-2">
                        <a href="{{route('post.like', $post->id)}}"><img src="{{asset('images/misc/heart.svg')}}" alt="" class="w-8"></a>
                        <span class="text-white rounded-full bg-green-400 text-center p-1">{{$post->likes}}</span>
                    </div>
                </div>

                <div class="comment-box mt-4">

                    <div class="comment-input flex p-2 border-b border-t border-white">
                        <form action="{{route('comment.store', $post->id)}}" method="POST" class="flex p-2 w-full">
                            @csrf
                            <input type="text" name="comment_{{$post->id}}" class="rounded-sm shadow-md w-full p-1 pl-2 bg-gray-100" placeholder="Drop a comment">
                            <div class="submit ml-2">
                                <label for="submit_comment_{{$post->id}}" class="cursor-pointer">
                                    <img src="{{asset('images/misc/send.svg')}}" alt="" class="w-8">
                                </label>
                                <input type="submit" class="hidden" id="submit_comment_{{$post->id}}">
                            </div>
                        </form>
                    </div>

                    <div class="comments mt-4 space-y-2 w-full flex flex-col items-center justify-center">

                        @if($post->comments->count() > 0)
                        @foreach($post->comments as $comment)
                        <div class="comment bg-gray-100 p-2 rounded-md shadow-md w-full">
                            <div class="header">
                                <div class="user-holder flex space-x-3 items-center">
                                    <img src="{{asset('images/misc/user.svg')}}" alt="" class="w-12">
                                    <div class="w-full">
                                        <p class="user-name font-bold text-lg text-gray-600">{{$comment->user->name}}</p>
                                        <p class="posted-date text-xs text-gray-600">{{$post->created_at->toFormattedDateString()}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="comment-text mt-2">
                                <span class="text-gray-600">{{$comment->comment}}</span>
                            </div>
                        </div>
                        @endforeach
                        @else
                            <span class="text-white">No comments...</span>
                        @endif

                    </div>

                </div>

            </div>

        </div>
        @endforeach

    </div>

@stop
