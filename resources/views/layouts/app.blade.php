<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    {{--style--}}
    <link rel="stylesheet" href="{{asset('css/tailwind.css')}}">

    {{--Fontawesome--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />

    <style>
        * {
            font-family: 'Nunito';
        }
    </style>
</head>
<body class="antialiased">

<div class="navigation w-full bg-gray-500 h-12 flex items-center shadow-md">

    <div class="w-1/2 h-12 flex items-center space-x-12 text-white">

        <div class="logo ml-2">
            <a href="/"><img src="{{asset('images/misc/logo.svg')}}" alt="" class="w-12"></a>
        </div>

        <ul class="flex space-x-5 font-bold text-lg">
            <li class="p-1 rounded-md @if (is_active('/')) shadow-md bg-blue-500 @endif"><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="p-1 rounded-md @if (is_active('account-posts')) shadow-md bg-blue-500 @endif"><a href="{{route('post.account.posts')}}"><i class="fas fa-file-alt"></i></a></li>
        </ul>

    </div>

    <div class="w-1/2 h-12 flex items-center justify-end text-white">

        <ul class="flex space-x-5 text-sm mr-2 underline">
            @if(!Auth::check())
            <li><a href="{{route('login')}}">Login</a></li>
            <li><a href="{{route('register')}}">Register</a></li>
            @else
            <li><a href="{{route('post.account.posts')}}">My Posts</a></li>
            <li><a
                    href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"
                ><i class="fas fa-sign-out-alt text-lg text-red-500"></i></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            @endif
        </ul>

    </div>

</div>

<div class="main-content bg-gray-100 p-2 h-screen">

    @yield('main-content')

</div>

</body>
</html>
