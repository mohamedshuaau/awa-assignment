@extends('layouts.app')

@section('main-content')
    <div class="row justify-content-center flex flex-col items-center justify-center h-screen">
        <div class="col-md-8 bg-gray-500 p-5 text-white rounded-md shadow-md">
            <div class="card">
                <div class="card-header font-bold text-xl">{{ __('Register') }}</div>

                <div class="card-body mt-4">
                    <form method="POST" action="{{ route('register') }}" class="space-y-4">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right font-bold">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input placeholder="Enter your Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror p-1 rounded-md text-black" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right font-bold">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input placeholder="Enter your Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror p-1 rounded-md text-black" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right font-bold">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input placeholder="Enter your Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror p-1 rounded-md text-black" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right font-bold">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input placeholder="Confirm your Password" id="password-confirm" type="password" class="form-control p-1 rounded-md text-black" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary p-1 bg-green-500 text-white rounded-md shadow-md">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
