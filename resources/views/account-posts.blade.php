@extends('layouts.app')


@section('main-content')

    <div class="flex flex-col h-screen bg-gray-100">

        <div class="alerts w-1/2">
            @if(session()->has('updated'))
                <div class="success-alert text-lg w-full bg-green-500 text-white p-2 rounded-md shadow-md">{{session()->get('updated')}}</div>
            @endif

            @if(session()->has('warning'))
                <div class="warning-alert text-lg w-full bg-yellow-500 text-white p-2 rounded-md shadow-md">{{session()->get('warning')}}</div>
            @endif

            @if(session()->has('error'))
                <div class="danger-alert text-lg w-full bg-red-500 text-white p-2 rounded-md shadow-md">{{session()->get('error')}}</div>
            @endif

            @if ($errors->any())
                <div class="warning-alert text-lg w-full bg-yellow-500 text-white p-2 rounded-md shadow-md">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
        </div>

        <div class="mb-6 flex flex-col">
            <span class="font-bold text-2xl text-gray-700">Feel free to edit, view or delete your posts</span>
            <span class="font-bold text-md text-gray-700">All your posts are arranged for you by latest and categorized depending on the content</span>
        </div>

        <div class="card-wrapper flex flex-col bg-gray-100 mt-6">

            @if($posts->count() > 0)
                <span class="text-lg font-bold underline text-gray-700">Posts with Image and Content</span>
            @endif

            <div class="flex flex-wrap space-y-4 w-full space-x-4 bg-gray-100">
                @foreach($posts as $post)
                    <div class="single-post bg-gray-500 lg:w-1/4 w-full rounded-md shadow-md p-3 h-auto">

                        <div class="content mt-2">

                            <a href="{{route('post.account.posts.destroy', $post->id)}}">
                                <span class="border border-red-500 text-red-500 rounded-full p-1 w-8 h-8 float-right text-center mb-2">x</span>
                            </a>

                            <img src="{{asset('images/uploads/')}}/{{$post->image}}" alt="" class="mb-2 rounded-md">

                            <form action="{{route('post.account.posts.update', $post->id)}}" method="POST" enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                <textarea name="content_{{$post->id}}" class="rounded-sm shadow-md w-full p-1 pl-2 bg-gray-100" placeholder="Update your post">{{$post->content}}</textarea>

                                <div class="image-selection">
                                    <label for="image_{{$post->id}}">
                                        <img src="{{asset('images/misc/image_selection.svg')}}" alt="" class="w-8 cursor-pointer">
                                    </label>
                                    <input name="image_{{$post->id}}" type="file" id="image_{{$post->id}}" class="hidden">
                                </div>

                                <div class="post mt-4">
                                    <label for="submit_{{$post->id}}" class="p-1 bg-green-500 text-white rounded-md shadow-md cursor-pointer">Update</label>
                                    <input name="submit_{{$post->id}}" type="submit" id="submit_{{$post->id}}" value="Post" class="bg-blue-500 p-1 text-white rounded-md hidden">
                                </div>

                            </form>

                        </div>

                    </div>
                @endforeach
            </div>


        </div>

        <div class="card-wrapper flex flex-col bg-gray-100 mt-6">

            @if($posts_with_content->count() > 0)
                <span class="text-lg font-bold underline text-gray-700">Posts with Only Content</span>
            @endif

            <div class="flex flex-wrap space-y-4 w-full space-x-4 bg-gray-100">
                @foreach($posts_with_content as $post)
                    <div class="single-post bg-gray-500 lg:w-1/4 w-full rounded-md shadow-md p-3 h-auto">

                        <div class="content mt-2">

                            <a href="{{route('post.account.posts.destroy', $post->id)}}">
                                <span class="border border-red-500 text-red-500 rounded-full p-1 w-8 h-8 float-right text-center mb-2">x</span>
                            </a>

                            <img src="{{asset('images/uploads/')}}/{{$post->image}}" alt="" class="mb-2 rounded-md">

                            <form action="{{route('post.account.posts.update', $post->id)}}" method="POST" enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                <textarea name="content_{{$post->id}}" class="rounded-sm shadow-md w-full p-1 pl-2 bg-gray-100" placeholder="Update your post">{{$post->content}}</textarea>

                                <div class="image-selection">
                                    <label for="image_{{$post->id}}">
                                        <img src="{{asset('images/misc/image_selection.svg')}}" alt="" class="w-8 cursor-pointer">
                                    </label>
                                    <input name="image_{{$post->id}}" type="file" id="image_{{$post->id}}" class="hidden">
                                </div>

                                <div class="post mt-4">
                                    <label for="submit_{{$post->id}}" class="p-1 bg-green-500 text-white rounded-md shadow-md cursor-pointer">Update</label>
                                    <input name="submit_{{$post->id}}" type="submit" id="submit_{{$post->id}}" value="Post" class="bg-blue-500 p-1 text-white rounded-md hidden">
                                </div>

                            </form>

                        </div>

                    </div>
                @endforeach
            </div>


        </div>

        <div class="card-wrapper flex flex-col bg-gray-100 mt-6">

            @if($posts_with_images->count() > 0)
                <span class="text-lg font-bold underline text-gray-700">Posts with Only Image</span>
            @endif

            <div class="flex flex-wrap space-y-4 w-full space-x-4 bg-gray-100">
                @foreach($posts_with_images as $post)
                    <div class="single-post bg-gray-500 lg:w-1/4 w-full rounded-md shadow-md p-3 h-auto">

                        <div class="content mt-2">

                            <a href="{{route('post.account.posts.destroy', $post->id)}}">
                                <span class="border border-red-500 text-red-500 rounded-full p-1 w-8 h-8 float-right text-center mb-2">x</span>
                            </a>

                            <img src="{{asset('images/uploads/')}}/{{$post->image}}" alt="" class="mb-2 rounded-md">

                            <form action="{{route('post.account.posts.update', $post->id)}}" method="POST" enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                <textarea name="content_{{$post->id}}" class="rounded-sm shadow-md w-full p-1 pl-2 bg-gray-100" placeholder="Update your post">{{$post->content}}</textarea>

                                <div class="image-selection">
                                    <label for="image_{{$post->id}}">
                                        <img src="{{asset('images/misc/image_selection.svg')}}" alt="" class="w-8 cursor-pointer">
                                    </label>
                                    <input name="image_{{$post->id}}" type="file" id="image_{{$post->id}}" class="hidden">
                                </div>

                                <div class="post mt-4">
                                    <label for="submit_{{$post->id}}" class="p-1 bg-green-500 text-white rounded-md shadow-md cursor-pointer">Update</label>
                                    <input name="submit_{{$post->id}}" type="submit" id="submit_{{$post->id}}" value="Post" class="bg-blue-500 p-1 text-white rounded-md hidden">
                                </div>

                            </form>

                        </div>

                    </div>
                @endforeach
            </div>


        </div>

    </div>

@stop
