<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;
use function Sodium\increment;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts_with_images = Post::where('user_id', auth()->user()->id)->where('image', '!=', null)->where('content', null)->latest()->get();
        $posts_with_content = Post::where('user_id', auth()->user()->id)->where('image', null)->where('content', '!=', null)->latest()->get();
        $posts = Post::where('user_id', auth()->user()->id)->where('image', '!=', null)->where('content', '!=', null)->latest()->get();
        return view('account-posts', compact('posts', 'posts_with_images', 'posts_with_content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'content' => 'nullable|max:255',
            'image' => 'nullable|max:5000'
        ]);

        if($request->image == null && $request->input('content') == null) {
            return redirect()->back()->with('warning', 'Either Image or Content is required'); //not an exception
        }

        $post = new Post;

        $post->user_id = auth()->user()->id;
        $post->content = $request->input('content'); //cant access content via $request->content

        if($request->has('image')) {
            $image = $request->file('image');

            $extension = $image->getClientOriginalExtension();
            $rename = Uuid::uuid4().'.'.$extension;

            $make_image = Image::make($image)->fit(1280);

            $make_image->save(public_path('images/uploads/'.$rename), 100);

            $post->image = $rename;
        }

        $post->save();

        return redirect()->back()->with('posted', 'Your post was posted successfully');

    }

    public function like($post_id) {

        $post = Post::findOrFail($post_id);

        $likes = Like::where('user_id', auth()->user()->id)->where('post_id', $post_id)->exists();

        if(!$likes) {
            $like = new Like;

            $like->user_id = auth()->user()->id;
            $like->post_id = $post->id;

            $like->save();

            $post->likes = $post->likes + 1;

            $post->update();

            return redirect(url()->previous().'#post_'.$post_id);
        }

        return redirect(url()->previous().'#post_'.$post_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'content_'.$post->id => 'nullable|max:255',
            'image_'.$post->id => 'nullable|max:5000'
        ]);

        $post->user_id = auth()->user()->id;
        $post->content = $request->input('content_'.$post->id);

        if($request->has('image_'.$post->id)) {
            $image = $request->file('image_'.$post->id);

            $extension = $image->getClientOriginalExtension();
            $rename = Uuid::uuid4().'.'.$extension;

            $make_image = Image::make($image)->fit(1280);

            $make_image->save(public_path('images/uploads/'.$rename), 100);

            $post->image = $rename;
        }

        $post->update();

        return redirect()->back()->with('updated', 'Your post was updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->back()->with('warning', 'Deleted Post Successfully');
    }
}
